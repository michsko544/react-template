import './App.scss';

import React, { ReactElement } from 'react';

function App(): ReactElement {
  return <div className="App">Hello Modeview Admin UI</div>;
}

export default App;
